# Sxmrb

Ruby library for SXMO[1] user scripts.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sxmrb'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install sxmrb

Or use an inline bundle:

```ruby
require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'sxmrb'
end
```

## Usage

### Prompt for a value:

```ruby
value = Sxmrb.input(prompt: 'Amount?')
```

### Build a menu:

```ruby
def main_menu
  Sxmrb.echo(<<~'END') | Sxmrb.menu(prompt: 'Ledger')
    Add Transaction
    Balance
    Budget
    Quit
    END
end
```

### Wrap a system command:

```ruby
def ledger(file: @file, command: nil, rest: nil)
  Sxmrb.sh String.new('ledger').tap { |cmd|
    cmd << " -f #{file.inspect}" if file
    cmd << " #{command}"         if command
    cmd << " #{rest}"            if rest
  }
end
```

### Build a menu from a system command:

```ruby
def accounts(query: nil)
  ledger(command: 'accounts', rest: query)
end

def input_account
  (accounts | Sxmrb.menu(prompt: 'Account?')).to_s.chomp
end
```

### Auto-wrapped system commands

Missing methods will automatically wrap the corresponding system commands:

```ruby
Sxmrb.awk('{print $NF}')
# => sh('awk', '{print $NF}')

Sxmrb.column('-t', '-R2')
# => sh('column', '-t', '-R2')

Sxmrb.echo('Hello World')
# => sh('echo', 'Hello World')
```

## Development

After checking out the repo, run `bin/setup` to install
dependencies. Then, run `rake test` to run the tests. You can also run
`bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will create
a git tag for the version, push git commits and the created tag, and
push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at
https://gitlab.com/fjc/sxmrb.

## License

The gem is available as open source under the terms of the [MIT
License](https://opensource.org/licenses/MIT).

[1]: https://sr.ht/~mil/Sxmo/
