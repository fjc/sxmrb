# frozen_string_literal: true

require_relative 'lib/sxmrb/version'

Gem::Specification.new do |spec|
  spec.name          = 'sxmrb'
  spec.version       = Sxmrb::VERSION
  spec.authors       = ['Frank J. Cameron']
  spec.email         = ['fjc@fastmail.net']

  spec.summary       = 'Ruby library for SXMO user scripts.'
  spec.homepage      = 'https://gitlab.com/fjc/sxmrb'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.7.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency 'example-gem', '~> 1.0'

  spec.add_dependency 'shell'
  spec.add_dependency 'shellwords'

  # backport Symbol#name, but I didn't want to force a hard dependency
  # here for just one small piece of code; so, the code will try to load
  # it from the backports gem first and fall back to a simple monkey patch
  # spec.add_dependency 'backports', '>= 3.19.0'

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
