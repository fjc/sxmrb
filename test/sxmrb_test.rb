# frozen_string_literal: true

require 'test_helper'

class SxmrbTest < Minitest::Test
  def setup
    Sxmrb.renew_instance(shell: nil)
  end

  def test_that_it_has_a_version_number
    refute_nil ::Sxmrb::VERSION
  end

  def test_it_does_something_useful
    assert true
  end

  def test_it_wraps_a_shell
    default_class = ::Shell
    custom_shell  = Minitest::Mock.new

    Sxmrb.renew_instance
    assert_instance_of default_class, Sxmrb.instance.instance_variable_get(:@_sh)

    Sxmrb.renew_instance(shell: custom_shell)

    custom_shell.expect :system, 42, ['foo bar baz']
    Sxmrb.sh('foo bar baz')
    assert_mock custom_shell

    Sxmrb.renew_instance
    assert_instance_of default_class, Sxmrb.instance.instance_variable_get(:@_sh)
  end

  def test_it_provides_awk
    custom_shell = Minitest::Mock.new
    Sxmrb.renew_instance(shell: custom_shell)
    custom_shell.expect :system, nil, ['awk', 'bar baz']
    Sxmrb.awk('bar baz')
    assert_mock custom_shell
  end

  def test_it_provides_cat
    custom_shell = Minitest::Mock.new
    Sxmrb.renew_instance(shell: custom_shell)
    custom_shell.expect :system, nil, ['cat', 'bar baz']
    Sxmrb.cat('bar baz')
    assert_mock custom_shell
  end

  def test_it_provides_column
    custom_shell = Minitest::Mock.new
    Sxmrb.renew_instance(shell: custom_shell)
    custom_shell.expect :system, nil, ['column', 'bar baz']
    Sxmrb.column('bar baz')
    assert_mock custom_shell
  end

  def test_it_provides_echo
    custom_shell = Minitest::Mock.new
    Sxmrb.renew_instance(shell: custom_shell)
    custom_shell.expect :system, nil, ['echo', 'bar baz']
    Sxmrb.echo('bar baz')
    assert_mock custom_shell
  end

  def test_it_provides_tac
    custom_shell = Minitest::Mock.new
    Sxmrb.renew_instance(shell: custom_shell)
    custom_shell.expect :system, nil, ['tac', 'bar baz']
    Sxmrb.tac('bar baz')
    assert_mock custom_shell
  end
end
