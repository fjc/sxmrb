## [Unreleased]

## [0.2.0] - 2021-05-22

- Wrap "any" standard system commands automatically.

## [0.1.1] - 2021-05-18

- Wrap more standard system commands
  - awk
  - cat
  - column
  - tac

## [0.1.0] - 2021-05-16

- Initial release
