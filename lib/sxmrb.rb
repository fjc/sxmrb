# frozen_string_literal: true

require_relative 'sxmrb/version'

require 'shell'
require 'shellwords'

require 'singleton'
require 'forwardable'

begin
  require 'backports/3.0.0/symbol'
rescue LoadError
  unless Symbol.method_defined? :name
    # Symbol#name hack
    class Symbol
      alias name to_s
    end
  end
end

# SXMO user scripts in ruby.
class Sxmrb
  include ::Singleton

  # Normally when using Singleton there is no way to pass arguments
  # when instantiating.  The renew hack below allows the singleton
  # instance to be re-instantiated and can pass arguments at that time;
  # this was added as an experiment to assist with testing.
  def initialize(shell: ::Shell.new) # :nodoc:
    @_sh = shell
  end

  # Run a system shell command.
  #
  # @example
  #   sh('ls -tr').to_s.lines.last
  def sh(...)
    @_sh.system(...)
  rescue ::StandardError => e
    raise Error, e
  end

  # Display a menu using dmenu [X11] or vis-menu [ssh].
  # A newline-separated list of items is read from standard input.
  # The selected item is printed to standard output.
  #
  # @example
  #   file = (sh('ls') | menu(prompt: 'File?')).to_s.chomp
  # rubocop:disable Lint/AmbiguousOperator
  def menu(prompt: nil, options: [])
    sh *(['sxmo_dmenu_with_kb.sh'].tap { |cmd|
      cmd.push *options
      cmd.push '-p', prompt if prompt
    })
  end
  # rubocop:enable Lint/AmbiguousOperator

  # Use a blank #menu to prompt for a value.
  #
  # @example
  #   value = input(prompt: 'Amount?').to_i
  def input(prompt:)
    (echo | menu(prompt: prompt)).to_s.chomp
  end

  def method_missing(cmd, *args, &_block)
    sh(*([cmd.name] + args))
  end

  def respond_to_missing?(cmd, include_private = false)
    sh('which', cmd.name).empty? ? super : true
  end

  class <<self
    # This evil hack experiment is intended to allow swapping out the
    # Shell instance when testing.
    def renew_instance(...) # :nodoc:
      @singleton__mutex__.synchronize {
        @singleton__instance__ = new(...)
      }
    end

    # Forward instance methods to the Singleton instance; false here
    # excludes ancestor methods.
    extend ::Forwardable
    def_delegators :instance, *Sxmrb.instance_methods(false)
  end

  class Error < ::StandardError; end
end
